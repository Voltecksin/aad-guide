package com.example.aadpractice.webview

import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.example.aadpractice.Constants
import com.example.aadpractice.R
import kotlinx.android.synthetic.main.activity_web_view.*

class WebViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        loadUrl()
    }

    private fun loadUrl() {
        wvMain.settings.javaScriptEnabled = true
        wvMain.webViewClient = object : WebViewClient() {

            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                pbWebView.visibility = View.VISIBLE
            }

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                pbWebView.visibility = View.GONE
            }

        }

        val url = intent.getStringExtra(Constants.URL_TAG)
        if (url != "") {
            wvMain.loadUrl(url)
        }

    }
}
