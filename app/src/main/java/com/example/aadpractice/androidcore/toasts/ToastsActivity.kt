package com.example.aadpractice.androidcore.toasts

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.aadpractice.Constants
import com.example.aadpractice.R
import com.example.aadpractice.webview.WebViewActivity
import kotlinx.android.synthetic.main.activity_toasts.*

class ToastsActivity : AppCompatActivity() {

    private val toastUrl = "https://developer.android.com/guide/topics/ui/notifiers/toasts?hl=es"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_toasts)
        setButtons()
    }

    private fun setButtons() {
        btnLongToast.setOnClickListener {
            showToast(
                "This is a LONG duration toast",
                Toast.LENGTH_LONG
            )
        }
        btnShortToast.setOnClickListener {
            showToast(
                "This is a SHORT duration toast",
                Toast.LENGTH_SHORT
            )
        }

        btnCustomToast.setOnClickListener {
            val layout = layoutInflater.inflate(
                R.layout.custom_toast,
                findViewById(R.id.clCustomToastContainer)
            )
            val toastText: TextView = layout.findViewById(R.id.tvCustomToast)
            toastText.text = "This is a CUSTOM toast"

            with(Toast(this)) {
                duration = Toast.LENGTH_LONG
                view = layout
                show()
            }
        }

        btnTopToast.setOnClickListener {
            showToastWithGravity(Gravity.TOP)
        }

        btnStartToast.setOnClickListener {
            showToastWithGravity(Gravity.START)
        }

        btnEndToast.setOnClickListener {
            showToastWithGravity(Gravity.END)
        }

        btnBottomToast.setOnClickListener {
            showToastWithGravity(Gravity.BOTTOM)
        }

        tvToastGoToDocumentation.setOnClickListener{
            Intent(this,WebViewActivity::class.java).apply{
                putExtra(Constants.URL_TAG, toastUrl)
                startActivity(this)
            }
        }
    }

    private fun showToast(text: String, duration: Int) {
        Toast.makeText(this, text, duration).show()
    }

    private fun showToastWithGravity(gravity: Int) {
        val toastText: String = when (gravity) {
            Gravity.TOP -> "This is a TOP Gravity toast"
            Gravity.START -> "This is a START Gravity toast"
            Gravity.END -> "This is a END Gravity toast"
            Gravity.BOTTOM -> "This is a BOTTOM Gravity toast"
            else -> "This is a default toast"
        }

        Toast.makeText(this, toastText, Toast.LENGTH_SHORT).apply {
            setGravity(gravity, 0, 0)
            show()
        }
    }
}
