package com.example.aadpractice.main

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.aadpractice.R
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup

class SubTopicAdapter(groups: MutableList<MainTopic>) :
    ExpandableRecyclerViewAdapter<MainTopicViewHolder, SubTopicViewHolder>(groups) {

    override fun onCreateGroupViewHolder(parent: ViewGroup?, viewType: Int): MainTopicViewHolder =
        MainTopicViewHolder(
            LayoutInflater.from(parent?.context).inflate(
                R.layout.topic_item,
                parent,
                false
            )
        )

    override fun onCreateChildViewHolder(parent: ViewGroup?, viewType: Int): SubTopicViewHolder =
        SubTopicViewHolder(
            LayoutInflater.from(parent?.context).inflate(
                R.layout.subtopic_item,
                parent,
                false
            )
        )

    override fun onBindChildViewHolder(
        holder: SubTopicViewHolder?,
        flatPosition: Int,
        group: ExpandableGroup<*>?,
        childIndex: Int
    ) {
        (group?.items?.get(childIndex) as SubTopic).run { holder?.bind(this) }
    }

    override fun onBindGroupViewHolder(
        holder: MainTopicViewHolder?,
        flatPosition: Int,
        group: ExpandableGroup<*>?
    ) {
        (group as MainTopic).run { holder?.bind(this) }
    }
}