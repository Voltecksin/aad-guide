package com.example.aadpractice.main

import android.view.View
import android.view.animation.Animation.RELATIVE_TO_SELF
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.TextView
import com.example.aadpractice.main.MainTopic
import com.example.aadpractice.R
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder

class MainTopicViewHolder(itemView: View) : GroupViewHolder(itemView) {

    private var tvMainTopic: TextView = itemView.findViewById(R.id.tvTopic)
    var ivArrow: ImageView = itemView.findViewById(R.id.ivMIArrow)

    fun bind(mainTopic: MainTopic) {
        tvMainTopic.text = mainTopic.title
    }

    override fun expand() {
        animateExpand()
    }

    override fun collapse() {
        animateCollapse()
    }

    private fun animateExpand() {
        val rotate = RotateAnimation(360f, 180f, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f)
        rotate.duration = 300
        rotate.fillAfter = true
        ivArrow.animation = rotate
    }

    private fun animateCollapse() {
        val rotate = RotateAnimation(180f, 360f, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f)
        rotate.duration = 300
        rotate.fillAfter = true
        ivArrow.animation = rotate
    }
}