package com.example.aadpractice.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.aadpractice.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val mainTopics: MutableList<MainTopic> = mutableListOf()
    private val mainTopicsString by lazy { resources.getStringArray(R.array.main_topics) }
    private val androidCoreSubTopicsString by lazy { resources.getStringArray(R.array.android_core_subtopics) }
    private val userInterfaceSubTopicsString by lazy { resources.getStringArray(R.array.user_interface_subtopics) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        populateMainTopicsList()
        setAdapter()
    }

    /**
     * This implementation must be improved
     */
    private fun populateMainTopicsList() {
        mainTopics.add(
            createMainTopic(
                mainTopicsString[0],
                createSubTopicList(androidCoreSubTopicsString.toList())
            )
        )
        mainTopics.add(
            createMainTopic(
                mainTopicsString[1],
                createSubTopicList(userInterfaceSubTopicsString.toList())
            )
        )
    }

    private fun createMainTopic(title: String, subTopics: MutableList<SubTopic>) =
        MainTopic(title, subTopics)

    private fun createSubTopicList(subtopicsTitles: List<String>): MutableList<SubTopic> {
        val subTopicList: MutableList<SubTopic> = mutableListOf()
        subtopicsTitles.forEach { subTopicList.add(SubTopic(it)) }
        return subTopicList
    }

    private fun setAdapter() {
        rvMATopics.adapter = SubTopicAdapter(mainTopics)
    }
}
