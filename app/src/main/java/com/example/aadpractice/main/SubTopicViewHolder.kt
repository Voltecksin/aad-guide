package com.example.aadpractice.main

import android.content.Intent
import android.view.View
import android.widget.TextView
import com.example.aadpractice.R
import com.example.aadpractice.androidcore.toasts.ToastsActivity
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder

class SubTopicViewHolder(itemView: View) : ChildViewHolder(itemView) {

    var tvSubTopic: TextView = itemView.findViewById(R.id.tvSubTopic)

    fun bind(subTopic: SubTopic) {
        tvSubTopic.text = subTopic.topicName

        if(tvSubTopic.text.toString() == "Toasts"){
            tvSubTopic.setOnClickListener { startActivity(ToastsActivity::class.java) }
        }
//        when (tvSubTopic.text.toString()) {
//            "Toast" -> tvSubTopic.setOnClickListener { startActivity(ToastsActivity::class.java) }
//        }

    }

    private fun startActivity(javaClass: Any) {
        tvSubTopic.context.startActivity(Intent(tvSubTopic.context, javaClass as Class<*>))
    }
}