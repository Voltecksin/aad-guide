package com.example.aadpractice.main

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup

class MainTopic(title: String?, items: MutableList<SubTopic>?) :
    ExpandableGroup<SubTopic>(title, items) {
}