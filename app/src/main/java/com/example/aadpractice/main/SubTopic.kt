package com.example.aadpractice.main

import android.os.Parcel
import android.os.Parcelable

class SubTopic(var topicName: String?) : Parcelable {

    constructor(parcel: Parcel) : this(parcel.readString())

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(topicName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SubTopic> {
        override fun createFromParcel(parcel: Parcel): SubTopic {
            return SubTopic(parcel)
        }

        override fun newArray(size: Int): Array<SubTopic?> {
            return arrayOfNulls(size)
        }
    }
}